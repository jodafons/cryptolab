echo "Installing the randomness NIST test package..."
cd RandomnessTest
make -j4
cd ../


echo "Installing the PyKCS11..."
cd pykcs11
sudo python3 setup.py install
rm -rf PyKCS11.egg-info
rm -rf dist
rm -rf build
cd ..

echo "Installing the Gaugi..."
cd Gaugi
source setup.sh
cd ..


echo "Installing the cryptolab..."
cd cryptolab
source setup.sh
cd ..

