
# Installing qualimeter library
echo "Rebuilding gaugi library..."
sudo python3 setup.py install

# Removing temps
echo "Removing cache files..."
sudo rm -rf gaugi.egg-info
sudo rm -rf dist
sudo rm -rf build
