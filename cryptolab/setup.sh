# Exporting basepath
export CRYPTOLAB_BASEPATH=${PWD}

# Installing qualimeter library
echo "Rebuilding qualimeter library..."
sudo python3 setup.py install

# Removing temps
echo "Removing cache files..."
sudo rm -rf cryptolab.egg-info
sudo rm -rf dist
sudo rm -rf build
