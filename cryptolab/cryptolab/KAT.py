
__all__ = ['KAT']

from Gaugi import Logger, EnumStringification, NotSet
from Gaugi import StatusCode, StatusTool
from Gaugi.messenger.macros import *

# Base class used for all tools for this framework
class KAT( Logger ):

  def __init__(self, name):
    Logger.__init__(self)
    self._name          = name
    self._status        = StatusTool.ENABLE
    self._initialized   = StatusTool.NOT_INITIALIZED
    self._finalized     = StatusTool.NOT_FINALIZED
    self._context       = NotSet
    self._backend       = NotSet

  def name(self):
    return self._name

  def setContext( self, context ):
    self._context = context

  def getContext(self):
    return self._context

  def setBackend(self, v):
    self._backend = v

  def getBackend(self):
    return self._backend

  def backend(self):
    return self._backend

  def initialize(self):
    self.enable()
    return StatusCode.SUCCESS

  def execute(self, context):
    self.setContext(context)
    return StatusCode.SUCCESS

  def finalize(self):
    return StatusCode.SUCCESS

  def status(self):
    return self._status

  def disable(self):
    MSG_INFO( self, 'Disable %s tool service.',self._name)
    self._status = StatusTool.DISABLE

  def enable(self):
    MSG_INFO( self, 'Enable %s tool service.',self._name)
    self._status = StatusTool.ENABLE

  def init_lock(self):
    self._initialized = StatusTool.IS_INITIALIZED

  def fina_lock(self):
    self._finalized = StatusTool.IS_FINALIZED

  def isInitialized(self):
    if self._initialized is StatusTool.IS_INITIALIZED:
      return True
    else:
      return False

  def isFinalized(self):
    if self._finalized is StatusTool.IS_FINALIZED:
      return True
    else:
      return False






