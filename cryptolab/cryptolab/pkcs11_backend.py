
__all__ = ['Backend', 'PyKCS11Backend']

from Gaugi import EnumStringification
from Gaugi.messenger import Logger

class Backend(EnumStringification):
  PyKCS11 = 0



class PyKCS11Backend( Logger ):
  def __init__(self, lib):
    Logger.__init__(self)
    try:
      from PyKCS11 import PyKCS11Lib
    except:
      self._logger.fatal("PyKCS11 module not found.")
    self._pkcs11 = PyKCS11Lib()
    self._pkcs11.load(lib)
    slot = self._pkcs11.getSlotList(tokenPresent=True)[0]
    self._session = self._pkcs11.openSession(slot, CKF_SERIAL_SESSION | CKF_RW_SESSION)
    self._core = Backend.PyKCS11


  def session(self):
    return self._session

  def pkcs11(self):
    return self._pkcs11
  
  def core(self):
    return self._core

