


__all__ = ["Roles", "ShaAlg", "KeyType", "BlockCipherMode"]

from Gaigi import EnumStringification


class Roles(EnumStringification):
  USER = 0
  SO   = 1

class ShaAlg(EnumStringification):
  SHA_1  = 0
  SHA256 = 1
  SHA384 = 2
  SHA512 = 3

class KeyType(EnumStringification):
  DES   = 0
  DES2  = 1
  DES3  = 2
  AES   = 3
  RSA   = 4

class BlockCipherMode(EnumStringification):
  DES_ECB   = 0
  DES_CBC   = 1
  DES2_ECB  = 2
  DES2_CBC  = 3
  DES3_ECB  = 4
  DES3_CBC  = 5
  AES_CBC   = 6
  AES_ECB   = 7



