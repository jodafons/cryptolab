#coding: utf-8
__all__ = ["bytevec2strhex", "strhex2bytevec","bytevec2strbits"]


def bytevec2strhex(l):
  s=str()
  for ll in l: s+=b''+str(hex(ll)).replace('0x','')
  return s

def strhex2bytevec(s):
  import numpy as np
  ss = np.split(np.array(list(s)),len(s)/2.)
  l=[]
  #return [ eval('0x'+c[0]+c[1])  for c in ss]
  for c in ss:
    l.append( eval('0x'+c[0]+c[1]) )
  return l

def bytevec2strbits(l):
  bits = str()
  for byte_int in l:
    bits+=bin(byte_int)[2:].zfill(8)
  return bits



