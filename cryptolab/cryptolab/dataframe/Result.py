__all__ = ["TestResult"]

from cryptolab.core import Logger, NotSet
from cryptolab.core import StatusCode
from cryptolab.dataframe import EDM

class TestResult(EDM):

  def __init__(self):
    Logger.__init__(self) 


  def execute(self):
    return StatusCode.SUCCESS

  def initialize(self):
    return StatusCode.SUCCESS

  def finalize(self):
    return StatusCode.SUCCESS




 

