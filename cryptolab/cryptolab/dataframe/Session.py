__all__ = ["Session"]

from cryptolab.core       import Logger, NotSet
from cryptolab.core       import StatusCode, Backend, Roles, ShaAlg, KeyType, BlockCipherMode
from cryptolab.dataframe  import EDM
from cryptolab.util       import *

class Session(EDM):

  def __init__(self, backend):
    Logger.__init__(self)
    self._backend = backend


  def execute(self):
    return StatusCode.SUCCESS

  def initialize(self):
    pin = self.getContext().getHandler("Roles").pin()
    self._logger.info("Login into the module as User")
    self.login( pin )
    return StatusCode.SUCCESS

  def finalize(self):
    return StatusCode.SUCCESS


  def session(self):
    return self._backend.session()


  def login( self, pin, user_type = Roles.USER ):

    if self._backend.core() is Backend.PyKCS11:
      from PyKCS11 import CKU_USER, CKU_SO
      if user_type is Roles.USER:
        #self._backend.session().login( pin, CKU_USER )
        self.session().login( pin )
      elif user_type is Roles.SO:
        #self._backend.session().login( pin, CKU_SO )
        self.session().login( pin )
      else:
        self._logger.fatal("Role not supported")
    #elif self._backend.core() is Backend.OtherBackend:
    # include the code here...
    else:
      self._fatal("Backend not supported")



  def digest( self, message, alg ):

    if self._backend.core() is Backend.PyKCS11:
      from PyKCS11 import Mechanism, CKM_SHA_1, CKM_SHA256, CKM_SHA512
      if alg is ShaAlg.SHA_1:
        mecha = Mechanism(CKM_SHA_1, None)
      elif alg is ShaAlg.SHA256:
        mecha = Mechanism(CKM_SHA256, None)
      elif alg is ShaAlg.SHA512:
        mecha = Mechanism(CKM_SHA512, None)
      else:
        self._logger.fatal("Sha algorithm not supported")
      digested = bytevec2strhex( self._backend.session().digest(strhex2bytevec(message), mecha ))
      return digested

    #elif self._backend.core() is Backend.OtherBackend:
    # include the code here...
    else:
      self._fatal("Backend not supported")



  def create_symm_key_object( self, id, key, alg ):

    if self._backend.core() is Backend.PyKCS11:
      import PyKCS11
      if alg is KeyType.DES:
        key_type = PyKCS11.CKK_DES
      elif alg is KeyType.DES3:
        key_type = PyKCS11.CKK_DES3
      else:
        self._logger.fatal("Key type not supported.")
      keyID = (id,)
      from cryptolab.util import strhex2bytevec
      # get the operation
      #key =  strhex2bytevec(key)
      template = [
          (PyKCS11.CKA_CLASS, PyKCS11.CKO_SECRET_KEY),
          (PyKCS11.CKA_KEY_TYPE, key_type),
          #(PyKCS11.CKA_VALUE, '0123456789abcdef0123456789abcdef'),
          (PyKCS11.CKA_VALUE, key),
          #(PyKCS11.CKA_VALUE, key),
          #(PyKCS11.CKA_TOKEN, PyKCS11.CK_TRUE),
          #(PyKCS11.CKA_PRIVATE, PyKCS11.CK_FALSE),
          (PyKCS11.CKA_ENCRYPT, PyKCS11.CK_TRUE),
          (PyKCS11.CKA_DECRYPT, PyKCS11.CK_TRUE),
          #(PyKCS11.CKA_SIGN, PyKCS11.CK_FALSE),
          #(PyKCS11.CKA_VERIFY, PyKCS11.CK_FALSE),
          #(PyKCS11.CKA_VALUE_LEN, len(key)),
          #(PyKCS11.CKA_LABEL, "MY_AES_KEY"),
          (PyKCS11.CKA_ID, keyID),
          ]
      self.session().createObject( template )

    else:
      self._fatal("Backend not supported")




  def encrypt( self, id, message, alg, iv=None ):

    if self._backend.core() is Backend.PyKCS11:
      from PyKCS11 import CKM_DES_ECB, CKM_DES_CBC, CKM_DES3_ECB, CKM_DES3_CBC, CKA_ID, Mechanism
      if alg is BlockCipherMode.DES_ECB:
        alg_type = CKM_DES_ECB
      elif alg is BlockCipherMode.DES_CBC:
        alg_type = CKM_DES_CBC
      elif alg is BlockCipherMode.DES3_ECB:
        alg_type = CKM_DES3_ECB
      elif alg is BlockCipherMode.DES3_CBC:
        alg_type = CKM_DES3_CBC
      else:
        self._logger.fatal("BlockCipherMode algorithm not supported")

      from cryptolab.util import strhex2bytevec, bytevec2strhex
      key_handle = self.session().findObjects( [(CKA_ID, (id,))] )[0]
      mechanism = Mechanism( alg_type, iv )
      output = self.session().encrypt(key_handle, message, mechanism)
      return  bytevec2strhex(output)
    else:
      self._fatal("Backend not supported")


  def decrypt( self, id, message, alg, iv=None ):

    if self._backend.core() is Backend.PyKCS11:
      from PyKCS11 import CKM_DES_ECB, CKM_DES_CBC, CKM_DES3_ECB, CKM_DES3_CBC, CKA_ID, Mechanism
      if alg is BlockCipherMode.DES_ECB:
        alg_type = CKM_DES_ECB
      elif alg is BlockCipherMode.DES_CBC:
        alg_type = CKM_DES_CBC
      elif alg is BlockCipherMode.DES3_ECB:
        alg_type = CKM_DES3_ECB
      elif alg is BlockCipherMode.DES3_CBC:
        alg_type = CKM_DES3_CBC
      else:
        self._logger.fatal("BlockCipherMode algorithm not supported")

      from cryptolab.util import strhex2bytevec, bytevec2strhex
      key_handle = self.session().findObjects( [(CKA_ID, (id,))] )[0]
      mechanism = Mechanism( alg_type, iv )
      output = self.session().decrypt(key_handle, message, mechanism)
      return  bytevec2strhex(output)
    else:
      self._fatal("Backend not supported")



  def generate_random( self, n_bytes = 1 ):

    if self._backend.core() is Backend.PyKCS11:
      output = self.session().generateRandom(n_bytes)
      return  output
    else:
      self._fatal("Backend not supported")







 
