__all__ = ["Roles"]

from cryptolab.core import Logger, NotSet
from cryptolab.core import StatusCode
from cryptolab.dataframe import EDM

class Roles(EDM):

  def __init__(self, pin, puk):
    Logger.__init__(self)
    self._pin = pin
    self._puk = puk


  def execute(self):
    return StatusCode.SUCCESS

  def initialize(self):
    return StatusCode.SUCCESS

  def finalize(self):
    return StatusCode.SUCCESS


  def setPIN(self, v):
    self._pin = v

  def setPUK(self, v):
    self._puk = v

  def pin(self):
    return self._pin

  def puk(self):
    return self._puk


 

