
#__all__ = ['BlockCipherTest', 'des_ecb_kat', 'des_cbc_kat']
__all__ = []
#from cryptolab.core import Logger, EnumStringification, NotSet, KAT, KeyType, BlockCipherMode
#from cryptolab.core import StatusCode, StatusTool
#from cryptolab.util import *
#
#
#
#
## Base class used for all tools for this framewor
#class BlockCipherTest( KAT ):
#
#  def __init__(self, name, path = None ):
#    KAT.__init__(self,name)
#    self._key_type = NotSet
#    self._cipher_type = NotSet
#    self._path = path
#    self._tests = NotSet
#
#
#  def initialize(self):
#    self.enable()
#    from cryptolab.util import CVAPParser, KATReaderType
#    if self._path:
#      reader = CVAPParser()
#      reader.setReader( KATReaderType.Cipher )
#      self._tests, self._key_type, self._cipher_type = reader.load( self._path )
#    return StatusCode.SUCCESS
#
#
#  def execute(self, context):
#    self.setContext(context)
#    passed = self.kat_test(self._tests)
#    return StatusCode.SUCCESS
#
#  def setKeyType( self, v ):
#    self._key_type = v
#  
#  def setCipherType( self, v ):
#    self._cipher_type = v
#
#  def setTests( self, v):
#    self._tests = v
#
#  def kat_test(self, tests):
#    passed = True
#    session = self.getContext().getHandler("Session")
#
#    for form in tests:
#      count = form['count'][0]
#      # compose the key
#      key = form['key']
#      # the int vector
#      IV  = form['iv'] # Can be None
#      # the input
#      message = form['input']
#      # the answer
#      answer = form['output']
#      # create the pkcs11 key template
#      keyID = 0x01
#      # get the operation
#      mode = form['test']
#      self._logger.info(self.name())
#      self._logger.info('[%s]', mode.upper())
#      if len(key)>1:
#        for i in range(len(key)):
#          self._logger.info("KEY%d = %s", i, key[i])
#      else:
#        self._logger.info("KEY = %s", key[0])
#      if len(IV)>1:
#        for i in range(len(IV)):
#          self._logger.info("IV%d = %s", i, IV[i])
#      elif len(IV) == 1:
#        self._logger.info("IV = %s", IV[0])
#
#      if len(message)>1:
#        for i in range(len(message)):
#          self._logger.info("%s%d = %s", 'PLAINTEXT' if mode=='encrypt' else 'CIPHERTEXT',i,message[i])
#      else:
#        self._logger.info("%s = %s", 'PLAINTEXT' if mode=='encrypt' else 'CIPHERTEXT',message[0])
#
#      if len(answer)>1:
#        for i in range(len(answer)):
#          self._logger.info("%s%d = %s", 'CYPHERTEXT' if mode=='encrypt' else 'PLAINTEXT',i,answer[i])
#      else:
#        self._logger.info("%s = %s", 'CYPHERTEXT' if mode=='encrypt' else 'PLAINTEXT',answer[0])
#
#
#      def concat(l):
#        s=str()
#        for ls in l:
#          s+=ls
#        return s
#
#      message=concat(message)
#      key=key[0]
#      #iv = concat(IV)
#      #print self._key_type
#      #print self._cipher_type
#      print key
#      #self._key_type = KeyType.DES3
#      #self._cipher_type = BlockCipherMode.DES_CBC
#      session.create_symm_key_object( keyID, key, self._key_type )
#      
#      
#      for idx,  iv in enumerate(IV):
#        
#        # find the first secret key
#        if 'encrypt' in mode: # encrypt input
#          output = session.encrypt(keyID, message, self._cipher_type, iv)
#        else: # decrypt input
#          output = session.decrypt(keyID, message, self._cipher_type, iv)
#        print 'KEY = ',key, ' MSG = ', message,' IV ', iv, ' output =  ',output , ' KNOW ANSWER = ',answer[idx] 
#
#      if output != answer[idx]:
#        self._logger.warning("Test with count %s reproved", count)
#        passed = False
#
#
#    return passed
#
#
#
#
## Set DES here becouse there is no official NIST KAT file
#des_ecb_kat = BlockCipherTest( "DES_ECB")
#des_ecb_kat.setKeyType( KeyType.DES )
#des_ecb_kat.setCipherType( BlockCipherMode.DES_ECB )
#des_ecb_kat.setTests( [
#                        {'count':0, 'key': '00000000', 'input': '00000000', 'output':'f47bb46273b15eb5', 'mode':'encrypt', 'iv':None},
#                        {'count':0, 'key': '00000001', 'input': 'abcdef12adcb1265abcdf57abc839182', 
#                          'output':'e92214d63e844edfa8263c38045b9381adb6f68f223efb8cc3af9e6d1796e3', 'mode':'encrypt', 'iv':None},
#                        {'count':0, 'key': 'ab1bc7df',  'input': '12345678', 'output':'3f60664a94d2512e', 'mode':'decrypt','iv':None},
#                        {'count':0, 'key': 'bcdaf349' , 'input': 'ab1246bc1234abcdabdf1895bacf1892', 
#                          'output':'21719325c77d2cbac16bc9fa539e2eade930af15c631f81ca439e56e9899255', 'mode':'decrypt','iv':None},
#                       ] )
#
#
## Set DES here becouse there is no official NIST KAT file
#des_cbc_kat = BlockCipherTest( "DES_CBC")
#des_cbc_kat.setKeyType( KeyType.DES )
#des_cbc_kat.setCipherType( BlockCipherMode.DES_CBC )
#des_cbc_kat.setTests( [
#                        {'count': 0,'iv': 'ab12cf73','key': '00000000','output': '68de1b8a82e1f5e','input': '00000000','mode': 'encrypt'},
#                        {'count': 1,'iv': '12345678','key': '00000001','output': 'a59e122b8def78ab3dc3e4c42a4351e3a588def7c8b664261eb6e3512ccb9',
#                         'input': 'abcdef12adcb1265abcdf57abc839182','mode': 'encrypt'},
#                        {'count': 0,'iv': 'abdcf267','key': 'ab1bc7df','output': '5e2229f2e06719','input': '12345678','mode': 'decrypt'},
#                        {'count': 1,'iv': '1234abcd','key': 'bcdaf349','output': '1043a011a61f4fdea09f8c867a84cced8239c53d17ce5ab21fa30d8b1ab60',
#                         'input': 'ab1246bc1234abcdabdf1895bacf1892','mode': 'decrypt'},
#                      ] )
#


