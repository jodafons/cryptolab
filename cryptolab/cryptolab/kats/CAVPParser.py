
__all__ = ["CVAPParser", "KATReaderType"]

from Gaugi import EnumStringification, Logger

class KATReaderType(EnumStringification):
  SHA       = 0
  SHAMonte  = 1
  Cipher    = 2
  RSA       = 3
  Signature = 4



class CVAPParser( Logger ):

  def __init__(self):
    Logger.__init__(self)
    # Default reader mode
    self._reader_type = KATReaderType.SHA

  def setReader( self, v ):
    self._reader_type = v

  def getReader( self ):
    return self._reader_type

  def reader( self ):
    return self._reader_type


  def load( self, fname ):
    # reading the sha vector test file
    if self.reader() is KATReaderType.SHA:
      return self.__parser_sha_vector_test( fname )
    # reading the sha monte vector test file
    elif self.reader() is KATReaderType.SHAMonte:
      return self.__parser_sha_monte_vector_test( fname )
    elif self.reader() is KATReaderType.Cipher:
      return self.__parser_cipher_vector_test( fname )
    else:
      self._logger.fatal("KAT Reader mode not supported.")


  def __parser_cipher_vector_test( self, fname_test ):
    # CAVS 11.1
    # Config Info for : "tdes_values"
    # TDES Multi block Message Test for CBCI
    # State : Encrypt and Decrypt
    # Thu Apr 21 10:36:47 AM
    ##
    ##[ENCRYPT]
    ##
    ##COUNT = 0
    ##KEY1 = d0230d1c3eda264f
    ##KEY2 = e0946d3e1f231f4c
    ##KEY3 = fbfb1c7a7fd3aec4
    ##IV1 = ec40273d4b1fda76
    ##IV2 = 41957c92a0752fcb
    ##IV3 = 96ead1e7f5ca8520
    ##PLAINTEXT = 69583de887b72f22e3bf041fc0f53932ab02e9dfb0129e55
    ##CIPHERTEXT = 44ae31a69a5593a125d39b40e8230e72ad66d8a18ee8e250
    ##
    ##COUNT = 1
    def parser_line( s ):
      s =  s.replace('\n','').replace('\t','').replace('\r','').split(' ')
      return [s[0],s[2]] if len(s)==3 else s
    # loop over filei
    tests=[]
    is_encrypt_test=False
    lines = open(fname_test,'r').readlines()
    from copy import copy 
    def get_form():
      return {'count':[], 'key':[], 'iv':[], 'input':[], 'output':[], 'test':None}
  
    from cryptolab.core import KeyType, BlockCipherMode
    key_type = None; cipher_type=None
    form = None
    for line in lines:
      # Use this mechanism to try to get the algorithm ans mode from the
      # comment lines.
      if '#' in line:
        # Try to get the key type
        if 'tdes_values' in line:
          key_type = KeyType.DES3
        if 'AES' in line:
          key_type = KeyType.AES
        # Try to get the cipher mode
        if 'CBC' in line:
          cipher_str = 'CBC'
        if 'ECB' in line:
          cipher_str = 'ECB'
        continue
      # if is not a arg, parser this line to a  better shape
      line = parser_line(line)
      if line[0] == "[ENCRYPT]":
        is_encrypt_test=True; continue
      if line[0] == "[DECRYPT]":
        is_encrypt_test=False; continue
      # trigger
      if line[0] == '':
        # to avoid the first '' in the text file
        if form:
          form['test'] = 'encrypt' if is_encrypt_test else 'decrypt'
          tests.append( copy(form) )
        form = get_form()
      # Get the count test
      if 'COUNT'in line[0]:
        form['count'] = line[1]
      # Get the keys
      if 'KEY' in line[0]:
        form['key'].append(line[1])
      # Get the IVs
      if 'IV' in line[0]:
        form['iv'].append(line[1])
      # Get the input
      if ('PLAINTEXT' in line[0]) and is_encrypt_test:
        form['input'].append( line[1] )
      # Get the output
      if ('PLAINTEXT' in line[0]) and not is_encrypt_test:
        form['output'].append( line[1] )
      # Get the input
      if ('CIPHERTEXT' in line[0]) and not is_encrypt_test:
        form['input'].append( line[1] )
      # Get the output
      if ('CIPHERTEXT' in line[0]) and is_encrypt_test:
        form['output'].append( line[1] )

    # Retrive the cipher enumeration
    if key_type is KeyType.DES3 and 'CBC' in cipher_str: cipher_type = BlockCipherMode.DES3_CBC
    if key_type is KeyType.DES3 and 'ECB' in cipher_str: cipher_type = BlockCipherMode.DES3_ECB
    if key_type is KeyType.AES and 'CBC' in cipher_str:  cipher_type = BlockCipherMode.AES_ECB
    if key_type is KeyType.AES and 'ECB' in cipher_str:  cipher_type = BlockCipherMode.AES_ECB
    
    if not key_type:
      self._logger.warning("Key type not retrived from file.")
    if not cipher_type:
      self._logger.warning("Cipher mode type not retrived from file.")


    # Return the list of tests (in raw)
    return tests, key_type, cipher_type




  def __parser_sha_monte_vector_test( self, fname_test ):
    response = []
    def parser_line( s ):
      s =  s.replace('\n','').replace('\t','').replace('\r','').split(' ')
      return [s[0],s[2]] if len(s)==3 else s
    # loop over file
    sha_alg = None
    test_file = open(fname_test,'r')
    for line in test_file:
      if '#' in line:
        if 'SHA-1' in line:  sha_alg = ShaAlg.SHA_1
        if 'SHA-256' in line:  sha_alg = ShaAlg.SHA256
        if 'SHA-384' in line:  sha_alg = ShaAlg.SHA384
        if 'SHA-512' in line:  sha_alg = ShaAlg.SHA512
      line = parser_line(line)
      if line[0] == 'Seed':
        seed = line[1]
      if line[0] == 'MD':
        response.append(line[1])
    
    if not sha_alg:
      self._logger.warning("SHA algorithm  not retrived from file.")
    # Return the list of tests (in raw)
    return seed, response, sha_alg
  
  
  def __parser_sha_vector_test( self, fname_test ):
    tests = []
    def parser_line( s ):
      s =  s.replace('\n','').replace('\t','').replace('\r','').split(' ')
      return [s[0],s[2]] if len(s)==3 else s
    # loop over file
    sha_alg = None
    test_file = open(fname_test,'r')
    for line in test_file:

      if '#' in line:
        if 'SHA-1' in line:  sha_alg = ShaAlg.SHA_1
        if 'SHA-256' in line:  sha_alg = ShaAlg.SHA256
        if 'SHA-384' in line:  sha_alg = ShaAlg.SHA384
        if 'SHA-512' in line:  sha_alg = ShaAlg.SHA512

      line = parser_line(line)
      if line[0] == 'Msg':
        msg = line[1]
      if line[0] == 'MD':
        answer = line[1]
      tests.append( {"input":msg,'output':answer})
    # Return the list of tests (in raw)
    if not sha_alg:
      self._logger.warning("SHA algorithm  not retrived from file.")
    return tests, sha_alg












