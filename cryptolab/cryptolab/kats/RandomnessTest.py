
__all__ = ['RandomnessTest']

from cryptolab.core   import Logger, EnumStringification, NotSet, KAT
from cryptolab.core   import StatusCode, StatusTool, ShaAlg
from cryptolab.util   import *


# Base class used for all tools for this framework
class RandomnessTest( KAT ):

  def __init__(self, name):
    KAT.__init__(self,name)


  def initialize(self):
    self.enable()
    return StatusCode.SUCCESS


  def execute(self, context):

    self.setContext(context)
    self._logger.info('Prepare to test the Random generator')
    f = open("random_generator.txt",'w')
    session = context.getHandler("Session")
    from cryptolab.util.functions import progressbar
    for _ in progressbar(range(100), 'Generating...'):
      bits =  bytevec2strbits( session.generate_random(100) )
      f.write(bits)
    f.close()

    self._logger.info('Start the external tool...')
    import os
    self._logger.info("sts -v 1 -i 32 -I 1 -w . -F a random_generator.txt")
    os.system("sts -v 1 -i 32 -I 1 -w . -F a random_generator.txt")

    result = open('result.txt','r')
    for line in result:
      self._logger.info(line.replace('\n',''))

    return StatusCode.SUCCESS







