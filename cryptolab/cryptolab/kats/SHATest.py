
__all__ = ['SHATest']

from cryptolab.core   import Logger, EnumStringification, NotSet, KAT
from cryptolab.core   import StatusCode, StatusTool, ShaAlg
from cryptolab.util   import *


# Base class used for all tools for this framework
class SHATest( KAT ):

  def __init__(self, name, path ):
    KAT.__init__(self,name)
    self._path = path
    self._alg = NotSet
    self._monte_carlo = False


  def initialize(self):
    # Use this kat name as configuration parameter for resume test vector
    kat = self._path.split('/')[-1] # Get the txt/rsp file name
    self._monte_carlo = True if 'Monte' in kat else False

    from cryptolab.util import CVAPParser, KATReaderType
    reader = CVAPParser("Parser")

    if self._monte_carlo:
      reader.setReader( KATReaderType.SHAMonte )
      self._seed, self._know_answer, self._sha_alg = reader.load( self._path )
    else:
      reader.setReader( KATReaderType.SHA )
      self._tests, self._sha_alg = reader.load( self._path )

    self.enable()
    return StatusCode.SUCCESS


  def execute(self, context):
    self.setContext(context)
    if self._monte:
      passed = self.kat_monte_test(self._seed, self._know_answer, self._alg)
    else:
      passed = self.kat_test(self._tests, self._alg)
    return StatusCode.SUCCESS


  # See for reference: https://csrc.nist.gov/csrc/media/projects/cryptographic-algorithm-validation-program/documents/shs/shavs.pdf
  def kat_test_monte_carlo(self, seed, know_answer, alg = ShaAlg.SHA_1):
    passed=True
    session = self.getContext().getHandler("Session")

    from collections import deque
    self._logger.debug('%s : Seed = %s', self.name(), seed )
    for j in range(100):
      # MD1 = MD2 = MD3 = seed
      MD = deque( [seed,seed,seed] )
      for i in range(3,1003):
        M = MD[0]+MD[1]+MD[2]
        if i <= 7:  self._logger.debug( '%s : i%d = %s',self.name(),i,M )
        #Mi = bytevec2strhex( session.digest(strhex2bytevec(M)) )
        Mi = session.digest(M, alg)
        if i <= 7: self._logger.debug( '%s : MD%d = %s',self.name(),i,Mi)
        MD.append(Mi); MD.popleft()
      # jet the last message digest
      Mj = MD.pop()
      seed = Mj
      # output the KAT result
      if Mj != know_answer[j]:
        passed=False
        self._logger.warning('%s : MD%d = %s [ FAILED ]',self.name(), j, Mj)
      else:
        self._logger.info('%s : MD%d = %s [   OK   ]',self.name(), j, Mj)

    return passed



  # See for reference: https://csrc.nist.gov/csrc/media/projects/cryptographic-algorithm-validation-program/documents/shs/shavs.pdf
  def kat_test(self, tests, alg = ShaAlg.SHA_1):
    passed=True
    session = self.getContext().getHandler("Session")

    for form in self._tests:
      message = form['message']
      self._logger.info('%s : Msg = %s',self.name(), message)
      answer = form['know_answer']
      MD = session.digest(message, alg)
      # output the KAT result
      if MD != answer:
        passed=False
        self._logger.warning('%s : MD  = %s [ FAILED ]',self.name(), D)
      else:
        self._logger.info('%s : MD  = %s [   OK   ]',self.name(), D)


    return passed



