__all__ = ["EventContext"]

from Gaugi import Logger, NotSet
from Gaugi import StatusCode
from Gaugi.messenger.macros import *

class EventContext(Logger):

  def __init__(self, t):
    Logger.__init__(self) 
    import collections
    self._containers = collections.OrderedDict()
    self._decoration = dict()

  def setHandler(self, key, obj):
    if key in self._containers.keys():
      MSG_ERROR( self, "Key %s exist into the event context. Attach is not possible...",key)
    else:
      self._containers[key]=obj

  def getHandler(self,key):
    return NotSet if not key in self._containers.keys() else self._containers[key]


  def execute(self):
    return StatusCode.SUCCESS

  def initialize(self):
    for key, edm in self._containers.items():
      if edm.initialize().isFailure():
        MSG_WARNING( self,  'Can not initialize the edm %s', key )
 
    return StatusCode.SUCCESS

  def finalize(self):
    return StatusCode.SUCCESS

  def setDecor(self, key, v):
    self._decoration[key] = v

  def getDecor(self,key):
    try:
      return self._decoration[key]
    except KeyError:
      MSG_WARNING( self, 'Decoration %s not found',key)

  def clearDecorations(self):
    self._decoration = dict()

  def decorations(self):
    return self._decoration.keys()






