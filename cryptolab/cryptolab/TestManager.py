
__all__ = ['TestManager']

from Gaugi.messenger import Logger, LoggingLevel
from Gaigi.messenger.macros import *
from Gaugi.types import NotSet, Backend
from Gaugi import StatusCode





# The main framework base class for SIM analysis.
# This class is responsible to build all containers object and
# manager the storegate and histogram services for all classes.
class TestManager( Logger ):


  def __init__(self, name, lib, pin, puk, level = logging.DEBUG, backend = Backend.PyKCS11):
    Logger.__init__(self, level=level)
    import collections
    self._tool = collections.OrderedDict()
    self._containers = {}
    self._context = NotSet
    self._name=name


    if backend is Backend.PyKCS11:
      MSG_INFO( self, "Using PyKCS11 as backend.")
      from cryptolab import PyKCS11Backend
      self._backend = PyKCS11Backend(lib)
    else:
      MSG_FATAL( self, "Backend not supported")

    self._pin = pin
    self._puk = puk

  
  def addTool(self, tool):
    from cryptolab import KAT
    if not issubclass(type(tool),KAT):
      MSG_FATAL( self, "This tool must have inheritance from Algorithm. Is this a service?")
    if not tool.name() in self._tool.keys():
      MSG_DEBUG( self, "Include %s into the stack",tool.name())
      self._tool[tool.name()] = tool
    else:
      MSG_FATAL( self, "Tool %s already exist into the tool service list. Please include this tool with another name...", tool.name())



  def initialize( self ):

    MSG_INFO( self, 'Initializing EventManager...')
    # create the event context. This will be used to hold all dataframes (EDMs)
    # produced during the execution loop. Its possible to attach the thread pointes
    from cryptolab import EventContext
    self._context = EventContext("EventContext")
    
    # Create here all dataframes 
    from cryptolab.dataframe import TestResult
    from cryptolab.dataframe import Roles
    from cryptolab.dataframe import Session

    self._containers = {
                        "Session"   : Session(self._backend),
                        "Roles"     : Roles(self._pin, self._puk),
                        "Result"    : TestResult(),
                       }

    # Attach all EDMs into the event context
    for key, edm in self._containers.items():
      edm.setContext(self.getContext())
      self.getContext().setHandler(key, edm)
  

    # Loop over secundary tool list.
    for key, tool in self._tool.items(): 
      if tool.isInitialized():continue
      MSG_INFO( self, 'Initializing with name: %s', key)
      self.getContext().setHandler( key , tool )
      tool.setContext( self.getContext() )
      tool.setBackend( self._backend )
      tool.level = self._level
      if tool.initialize().isFailure():
        MSG_FATAL( self, "Can not initialize %s.", key)
 
    if self.getContext().initialize().isFailure():
      MSG_FATAL( self, "Can not initialize Event Context.")

    MSG_INFO( self, "Event manager initialization completed.")
    return StatusCode.SUCCESS


  def execute(self):

    MSG_INFO( self, 'Running...')
    # Tools 
    for key, tool in self._tool.items(): 
      MSG_DEBUG( self, "Execute tool %s...",tool.name())
      if( tool.execute( self.getContext() ).isFailure() ):
        MSG_WARNING( self, "Impossible to execute %s.", tool.name())

    return StatusCode.SUCCESS


  def finalize(self):
    MSG_INFO( self, 'Finalizing StoreGate service...')
    self._session.logout()
    self._session.closeSession()
    # Tools 
    for key, tool in self._tool.items(): 
      if( tool.finalize().isFailure() ):
        MSG_WARNING( self, "Impossible to execute %s.", tool.name())
        return StatusCode.FAILURE

    return StatusCode.SUCCESS


  def getContext(self):
    return self._context

  def setContext(self, context):
    self._context = context













