
# Cryptographic Algorithm Validation Program 

The Cryptographic Algorithm Validation Program (CAVP) provides validation testing of FIPS-approved 
and NIST-recommended cryptographic algorithms and their individual components. Cryptographic algorithm 
validation is a prerequisite of cryptographic module validation. Vendors may use any of the NVLAP-accredited 
Cryptographic and Security Testing (CST) Laboratories to test algorithm implementations. For more information
see https://csrc.nist.gov/projects/cryptographic-algorithm-validation-program

An algorithm implementation successfully tested by a lab and validated by NIST is added 
to an appropriate validation list, which identifies the vendor, implementation, operational 
environment, validation date and algorithm details.

A cryptographic module validated to FIPS 140-2 shall implement at least one Approved security 
function used in an Approved mode of operation. For an algorithm implementation to be listed on 
a cryptographic module validation certificate as an Approved security function, the algorithm 
implementation must meet all the requirements of FIPS 140-2 and must successfully complete the 
cryptographic algorithm validation process. A product or implementation does not meet the FIPS 140-2 
applicability requirements by simply implementing an Approved security function and acquiring 
validations for each of the implemented algorithms. The program will folow the recomendations 
from ITI-Brasil using the MCT (Manual de Condutas Tecnicas in portuguese) as base.
To see the specifications for each type of module please see the links below:

- For smartcards: https://www.iti.gov.br/images/repositorio/legislacao/manuais-de-conduta-tecnica/MCT-1--Volume-I---versao-4.2Requisitos-Materiais-Documentos-cartoes.pdf
- For tokens: https://www.iti.gov.br/images/repositorio/legislacao/manuais-de-conduta-tecnica/MCT-3_-_Vol.1_-_V_3.1.pdf
- For HSMs: https://www.iti.gov.br/images/repositorio/legislacao/manuais-de-conduta-tecnica/MCT-7__Vol.1_V_2.2.pdf

