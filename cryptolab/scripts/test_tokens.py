
import os, glob
from cryptolab import *

# the pkcs11 library
lib = "/Applications/tokenadmin.app/Contents/Frameworks/libaetpkss.3.5.dylib"
# basepath
basepath = os.environ['CRYPTOLAB_BASEPATH']




random_kats = [
                # Randomness NIST test suite
                RandomnessTest("RandomnessTest")
              ]

resume_kats = [
                # Crypto resume tests
                # sha algorithm test, the configuration is geven by the txt/rsp file name
                SHATest( "SHA1Monte"       , basepath+'/vectors/cryptography_vectors/hashes/SHA1/SHA1Monte.txt'     ),
                SHATest( "SHA1ShortMsg"    , basepath+'/vectors/cryptography_vectors/hashes/SHA1/SHA1ShortMsg.rsp'  ),
                SHATest( "SHA1LongMsg"     , basepath+'/vectors/cryptography_vectors/hashes/SHA1/SHA1LongMsg.rsp'   ),
                SHATest( "SHA256Monte"     , basepath+'/vectors/cryptography_vectors/hashes/SHA2/SHA256Monte.txt'   ),
                SHATest( "SHA256ShortMsg"  , basepath+'/vectors/cryptography_vectors/hashes/SHA2/SHA256ShortMsg.rsp'),
                SHATest( "SHA256LongMsg"   , basepath+'/vectors/cryptography_vectors/hashes/SHA2/SHA256LongMsg.rsp' ),
                SHATest( "SHA384Monte"     , basepath+'/vectors/cryptography_vectors/hashes/SHA2/SHA384Monte.txt'   ),
                SHATest( "SHA384ShortMsg"  , basepath+'/vectors/cryptography_vectors/hashes/SHA2/SHA384ShortMsg.rsp'),
                SHATest( "SHA384LongMsg"   , basepath+'/vectors/cryptography_vectors/hashes/SHA2/SHA384LongMsg.rsp' ),
                SHATest( "SHA512Monte"     , basepath+'/vectors/cryptography_vectors/hashes/SHA2/SHA512Monte.txt'   ),
                SHATest( "SHA512ShortMsg"  , basepath+'/vectors/cryptography_vectors/hashes/SHA2/SHA512ShortMsg.rsp'),
                SHATest( "SHA512LongMsg"   , basepath+'/vectors/cryptography_vectors/hashes/SHA2/SHA5121LongMsg.rsp'),
              ]

# Cypher KAT list
des_kats = [
                des_ecb_kat,
                des_cbc_kat,
                ]


des3_kats = []
# Include all kats cyphers from 3DES/CBC file
path = basepath+'/vectors/cryptography_vectors/ciphers/3DES/CBC/*'
for f in glob.glob(path):
  kat_name = f.split('/')[-1].replace('rsp','').replace('txt','')
  des3_kats.append( BlockCipherTest( "3DES_CBC_"+kat_name , f ) )

# Include all kats cyphers from 3DES/ECB file
path = basepath+'/vectors/cryptography_vectors/ciphers/3DES/ECB/*'
for f in glob.glob(path):
  kat_name = f.split('/')[-1].replace('rsp','').replace('txt','')
  des_kats.append( BlockCipherTest( "3DES_ECB_"+kat_name , f ) )



kats = []
kats.extend( random_kats )
#kats.extend( resume_kats )
#kats.extend( des_kats )
#kats.extend( des3_kats )

for kat in kats:
  print kat.name()






manager = TestManager( "CMVP" , lib, '24751165', '24751165')
#
for kat in kats:
  manager.addTool( kat )

manager.initialize()
manager.execute()
manager.finalize()









