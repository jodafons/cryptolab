
import os
from cryptolab import *

# the pkcs11 library
lib = "/Applications/tokenadmin.app/Contents/Frameworks/libaetpkss.3.5.dylib"
# basepath
basepath = os.environ['CRYPTOLAB_PATH']




kats = [
          # Randomness NIST test suite
          RandomnessTest("RandomnessTest")
          # Crypto resume tests
          # sha algorithm test
          Resume( "SHA1Monte"       , basepath++'/vectors/cryptography_vectors/hashes/SHA1/SHA1Monte.txt'     ,True   ),
          Resume( "SHA1ShortMsg"    , basepath++'/vectors/cryptography_vectors/hashes/SHA1/SHA1ShortMsg.rsp'          ),
          Resume( "SHA1LongMsg"     , basepath++'/vectors/cryptography_vectors/hashes/SHA1/SHA1LongMsg.rsp'           ),
          Resume( "SHA256Monte"     , basepath++'/vectors/cryptography_vectors/hashes/SHA2/SHA256Monte.txt'    ,True  ),
          Resume( "SHA256ShortMsg"  , basepath++'/vectors/cryptography_vectors/hashes/SHA2/SHA256ShortMsg.rsp'        ),
          Resume( "SHA256LongMsg"   , basepath++'/vectors/cryptography_vectors/hashes/SHA2/SHA256LongMsg.rsp'         ),
          Resume( "SHA384Monte"     , basepath++'/vectors/cryptography_vectors/hashes/SHA2/SHA384Monte.txt'    ,True  ),
          Resume( "SHA384ShortMsg"  , basepath++'/vectors/cryptography_vectors/hashes/SHA2/SHA384ShortMsg.rsp'        ),
          Resume( "SHA384LongMsg"   , basepath++'/vectors/cryptography_vectors/hashes/SHA2/SHA384LongMsg.rsp'         ),
          Resume( "SHA512Monte"     , basepath++'/vectors/cryptography_vectors/hashes/SHA2/SHA512Monte.txt'    ,True  ),
          Resume( "SHA512ShortMsg"  , basepath++'/vectors/cryptography_vectors/hashes/SHA2/SHA512ShortMsg.rsp'        ),
          Resume( "SHA512LongMsg"   , basepath++'/vectors/cryptography_vectors/hashes/SHA2/SHA5121LongMsg.rsp'        ),

          # Block Cypher tests
          # DES algorith, tests
          BlockCypher( ""

      ]







manager = TestManager( "CMVP" , lib, '24751165', '24751165')

for ket in kats:
  manager.addTool( kat )

manager.initialize()
manager.execute()
manager.finalize()









