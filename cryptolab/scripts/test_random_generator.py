
import os, glob
from cryptolab import *

# the pkcs11 library
lib = "/Applications/tokenadmin.app/Contents/Frameworks/libaetpkss.3.5.dylib"

rng_tests = [
                # Randomness NIST test suite
                RandomnessTest("RandomnessTest")
              ]


manager = TestManager( "CMVP" , lib, '24751165', '24751165')
manager.addTool( rng_tests )

manager.initialize()
manager.execute()
manager.finalize()









