

from setuptools import setup, find_packages


import os
os.environ['CRYPTOLAB_PATH'] = os.getcwd()

setup(name='cryptolab',
      version='1.0',
      description='Framework for crypto tests',
      author='Joao Victor da Fonseca Pinto',
      author_email='joao@laspi.ufrj.br',
      #url='',
      packages=find_packages(exclude=['docs', 'share','README.md'])
		)









