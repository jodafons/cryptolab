[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
# Cryptographic Module Validation Program (Brasil)


On July 17, 1995, NIST established the Cryptographic Module Validation Program (CMVP) that validates 
cryptographic modules to Federal Information Processing Standards (FIPS)140-1, Security Requirements 
for Cryptographic Modules, and other FIPS cryptography based standards. FIPS 140-2, Security Requirements 
for Cryptographic Modules, was released on May 25, 2001 and supersedes FIPS 140-1. The CMVP is a joint 
effort between NIST and the Canadian Centre for Cyber Security (CCCS), a branch of the Communications 
Security Establishment (CSE).

Modules validated as conforming to FIPS 140-2 are accepted by the Federal Agencies of both countries for 
the protection of sensitive information.

Vendors of cryptographic modules use independent, accredited Cryptographic and Security Testing (CST) 
laboratories to test their modules. The CST laboratories use the Derived Test Requirements (DTR), Implementation 
Guidance (IG) and applicable CMVP programmatic guidance to test cryptographic modules against the applicable 
standards. NIST's Computer Security Division (CSD) and CCCS jointly serve as the Validation Authorities for 
the program, validating the test results and issuing certificates.

## Installation (Linux)
```sh
source setup.sh
```
## Cryptographic Module Validation Program Repository from NIST

NIST maintains record of validations performed under all cryptographic standard testing programs past 
and present. As new algorithm implementations are validated by NIST and CCCS they may be viewed using the 
search interface from https://csrc.nist.gov/Projects/cryptographic-algorithm-validation-program/validation-search.

